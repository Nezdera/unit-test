// src/mylib.js

module.exports = {
  sum: (a, b) => a + b,
  subtract: (a, b) => a - b,
  random: () => {
    return Math.random();
  },
  arrayGen: () => [1, 2, 3]
}