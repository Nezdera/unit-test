const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {  
  before(() => {
    console.log('spec 2');
  });

  it('Should return 2 when using sum function with a=1 and b=1', () => {
    const result = mylib.sum(2, 2);
    expect(result).to.equal(4);
  });

  it('Assert foo is not bar', () => {
    assert('foo' !== 'bar');
  });
});